﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class btnAddDatar : MonoBehaviour {

	[SerializeField]
	private GameObject PanelQuest;
	[SerializeField]
	private Button QuestButton;
	[SerializeField]
	private Transform grid;
	[SerializeField]
	private Button btnAdd;
	[SerializeField]
	private List <Button> btnList = new List<Button>();
	[SerializeField]
	private GameObject popup;
	[SerializeField]
	private GameObject panelDelete;
	private int i=0;
	void Start(){
		btnAdd.onClick.AddListener (onClick);
	}

	public void onClick(){
		GameObject panel = Instantiate (PanelQuest);// menambah panel
		panel.name = ""+i; // merubah nama panel
		panel.GetComponentInChildren<InputField> ().name = "" + i; // merubah nama InputField child panel
		panel.transform.SetParent (grid, false);// set parent dengan grid
		Transform panelQuestTranform = panel.transform;// membut rect transform panel
		Button btnID = Instantiate (QuestButton);//membuat button
		btnID.name = ""+i;
		btnID.GetComponentInChildren<Text> ().text = "" + (i+1);//menambah text pada button
		btnID.transform.SetParent (panelQuestTranform, false);//set parent dengan panel
		btnList.Add(btnID.GetComponent<Button>());
		if (i == 0) {
			popup.GetComponentInChildren<Text>().text = "Click number of Questions to delete Question";
			popup.SetActive (true);
		}
		i++;
		AddListeners ();
	}

	void AddListeners(){
		foreach (Button btn in btnList) {
			btn.onClick.AddListener (OnClickBtnQuest);
		}
	}

	void OnClickBtnQuest(){
		Text txt = panelDelete.GetComponentInChildren<Text>();
		string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
		txt.name = name;
		txt.text = "Questions No " + name;
		panelDelete.SetActive (true);
	}
}
