﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class fieldScript : MonoBehaviour {
	[SerializeField]
	private ToggleGroup btnGroup;
	[SerializeField]
	private GameObject popup;
	[SerializeField]
	private GameObject warning;
	private List <InputField> field = new List<InputField>();
	private GameObject[] obj;
	[SerializeField]
	private Sprite bgWhite;
	[SerializeField]
	private Sprite bgBlack;
	private RaycastHit2D hit;
	private bool turun;
	private bool datar;
	private string tgName;

	void Start () {
		obj = GameObject.FindGameObjectsWithTag ("field");
		for (int i = 0; i < obj.Length; i++) {
			field.Add (obj [i].GetComponent<InputField> ()); // mengambil komponen dan taruh di array field
			field [i].image.sprite = bgWhite; // mengganti backbround image
			ColorBlock black = field[i].colors;
			black.normalColor = Color.black;
			field [i].colors = black;
		}
		//memanggil AddFieldListeners agar dapat menggunakan fungsi onValueChanged
		AddFieldListeners ();
	}

	// onGUI dipanggil jika terdeteksi tombol keyboard di tekan
	void OnGUI()
	{
		if (Input.GetKeyDown(KeyCode.Backspace)) {
			
		}
	}
		
	void AddFieldListeners(){
		foreach (InputField fx in field) {
			fx.onValueChanged.AddListener (delegate {
				FieldValueChanged ();
			});
		}
	}
	void FieldValueChanged(){
		GameObject[] item = GameObject.FindGameObjectsWithTag("quest");

		//jika banyak quest field kurang dari 1
		if(item.Length<1){
			popup.GetComponentInChildren<Text>().text = "Add question first !!!";
			popup.SetActive(true);
			int id;
			id = int.Parse (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
			// menghapus text pada fieldTTS yang terpilih
			foreach (InputField fx in field) {
				if (fx.isFocused == true) {
					fx.text = "";
				}
			}
		}else{
			foreach (GameObject obj in item) {
				Debug.Log (obj.name);
			}
			// deteksi Tab nama yang aktif dan rubah warnanya
			foreach (Toggle tg in btnGroup.ActiveToggles()) {
				tgName = tg.name;
				break;
			}
			switch (tgName) {
			case "TabMendatar":
				{
					string idField = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
					//memilih field selebah kiri jika field[idField].text sudah terisi
					foreach(InputField fx in field){
						if (fx.text != "") {
							Text[] txt = fx.GetComponentsInChildren<Text> ();
							txt [1].text = item [0].name;
							int id = int.Parse (idField);
							field [id].Select ();
						}
					}

					break;
				}
			case "TabMenurun":
				{
					//memilih field dibawah jika field[idField].text sudah terisi
					string idField = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;

					foreach(InputField fx in field){
						if (fx.text != "") {
							Text[] txt = fx.GetComponentsInChildren<Text> ();
							txt [1].text = item [0].name;
							int id = int.Parse (idField);
							id += 11;
							field [id].Select ();
							id = 0;
						}
					}
					break;
				}
			default :
				{
					warning.GetComponentInChildren<Text> ().text = "Tab tidak terpilih";
					warning.SetActive (true);
					break;
				}
			}
		}
	}
	// Update is called once per frame
	void Update () {
		// set normal collor menjadi putih ketika field terisi
		foreach (InputField fx in field) {
			if (fx.text != "") {
				ColorBlock cw = fx.colors;
				cw.normalColor = Color.white;
				fx.colors = cw;
			} else {
				ColorBlock cw = fx.colors;
				cw.normalColor = Color.black;
				fx.colors = cw;
			}
		}
	}
}
