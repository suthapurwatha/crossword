﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class addfield : MonoBehaviour {

	[SerializeField]
	private Transform body;

	[SerializeField]
	private GameObject field;

	[SerializeField]
	private int banyak;

	[SerializeField]
	private GameObject panelDel;

	[SerializeField]
	private GameObject popUp;

	[SerializeField]
	private GameObject warning;

	string tgName;
	void Awake(){
		Screen.SetResolution (480, 800, true); // set resolutions
		panelDel.SetActive(false);
		popUp.SetActive (false);
		warning.SetActive (false);
		for (int i = 0; i < banyak; i++) {
			GameObject xfield = Instantiate (field);// mengkopi objek
			xfield.name = "" + (i+1); // menentukan nama
			xfield.transform.SetParent (body, false); // menentukan parent objek
		}
	}
}
